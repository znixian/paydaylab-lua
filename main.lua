
blt_debugger._debug_enable = blt_debugger.get_parameter("debug_enable") == "true"
blt_debugger._read_message_handlers = {}
blt_debugger._last_message = {}

local function reload()
	if Global.level_data.level then
		-- Reload the heist
		managers.game_play_central:restart_the_game()
	else
		-- Reload the main menu
		setup:load_start_menu()
	end
end

table.insert(blt_debugger._read_message_handlers, function(msg)
	if msg.op ~= "reload" then return end

	reload()
end)

function blt_debugger.lua_msg_callback(messages)
	local unhandled = {}
	for _, msg in ipairs(messages) do
		local decoded = json.decode(msg)
		for _, handler in pairs(blt_debugger._read_message_handlers) do
			handler(decoded)
		end
		blt_debugger._last_message = decoded
		-- if msg == "paydaylab.reload" then
			-- blt_debugger.send_message("Reloading now!")
			-- --blt_debugger.wait()
			-- reload()
		-- else
			-- log("Received unknown debug message: " .. msg)
		-- end
	end
end

if blt_debugger.is_loaded() and blt_debugger._debug_enable then
	dofile(ModPath .. "debugger.lua")
end
