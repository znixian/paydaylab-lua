
local breakpoints = {
}

local selected_line = {}
local requestable_tables = {}

local state = {
	paused = false,
	stepInto = false,
	stepOver = false,
	stepOver_Target = nil,
	stepOver_Target_Prev = nil,

	temporary_breakpoints = false,
}

local compiled_breakpoints

local function add_compiled_breakpoint(filename, line, breakpoint)
	compiled_breakpoints[line] = compiled_breakpoints[line] or {}
	compiled_breakpoints[line][filename] = breakpoint
end

local function compile_breakpoints()
	compiled_breakpoints = {}

	for _, breakpoint in pairs(breakpoints) do
		local filename = breakpoint.source
		local line = breakpoint.line

		add_compiled_breakpoint(filename, line, breakpoint)
	end
end

compile_breakpoints()

local function serrialize_variable(var, sendtable)
	local t = type(var)
	local out = { type = t }

	if t == "nil" or t == "boolean" or t == "number" or t == "string" then
		-- NaN check
		if var ~= var then var = "nan" end

		out.value = var
	elseif t == "function" then
		local info = debug.getinfo(var)
		if #info.source < 250 then
			out.source = info.source
		end
		out.line = info.linedefined
		out.name = info.name
		out.what = info.namewhat
	elseif t == "thread" then
		-- do nothing for now
	elseif t == "userdata" or t == "table" then
		-- Send over the metadata if it's a userdata object
		local target = var
		if t == "userdata" then
			target = getmetatable(var)
		end

		out.address = blt.structid(var)

		log("Sending table with address " .. out.address)

		if sendtable then
			out.contents = {}
			for name, value in pairs(target) do
				out.contents[name] = serrialize_variable(value)
			end
		else
			requestable_tables[out.address] = var
		end
	end

	return out
end

table.insert(blt_debugger._read_message_handlers, function(msg)
	if msg.op ~= "get-heap-table" then return end

	local address = msg.address
	local value = requestable_tables[address]

	log("Value: " .. tostring(value))
	assert(value, "Unrequested table " .. address)

	blt_debugger.send_message(json.encode({
		op = "heap-value",
		address = address,
		value = serrialize_variable(value, true)
	}))
end)

local function updateStack(frames, nextline)
	local info = debug.getinfo(frames + 2)

	local stack = {}
	while true do
		local index = frames + 2 + #stack
		local info = debug.getinfo(index)
		if info == nil then break end

		local line = (#stack == 0 and nextline) or info.currentline

		local locals, upvalues = {}, {}

		local i = 1
		while true do
			local name, value = debug.getlocal(index, i)
			i = i + 1
			if not name then break end
			locals[name] = serrialize_variable(value)
		end

		local i = 1
		while true do
			local name, value = debug.getupvalue(info.func, i)
			i = i + 1
			if not name then break end
			locals[name] = serrialize_variable(value)
		end

		table.insert(stack, 1, {
			source = info.source,
			["current-line"] = info.currentline,
			variables = locals,
			upvalues = upvalues,
		})
	end

	blt_debugger.send_message(json.encode({
		op = "session",
		stack = stack,
		enabled = true
	}))
end

local function suspended(frames, nextline)
	local info = debug.getinfo(frames + 2)

	-- TODO Fetch all the local variables

	updateStack(frames + 1, nextline)

	if state.temporary_breakpoints then
		state.temporary_breakpoints = false
		compile_breakpoints()
	end

	while true do
		if state.continue then
			state.continue = false
			break
		end
		if state.stepInto then
			state.stepInto = false
			state.pause = true -- break on the next line
			break
		end
		if state.stepOver then
			state.stepOver = false
			state.stepOver_Target = info.func
			state.stepOver_Target_Prev = debug.getinfo(frames + 3).func -- If we step into a return
			break
		end
		if state.runToCursor then
			state.runToCursor = false
			if not selected_line.source or not selected_line.line then
				log("Cannot run to line when line is not selected!")
				break
			end

			add_compiled_breakpoint(selected_line.source, selected_line.line, {
			})
			state.temporary_breakpoints = true

			break
		end

		blt_debugger.wait()
	end

	blt_debugger.send_message(json.encode({
		op = "session",
		enabled = false
	}))

	-- Clear out the hashes from last time to allow
	-- the GC to clean everything up
	requestable_tables = {}
end

local function hook_line(name, nextline)
	if compiled_breakpoints[nextline] then
		local info = debug.getinfo(2, "S")
		local name = info.source

		local bp = compiled_breakpoints[nextline][name]
		if not bp then return end

		suspended(1, nextline)
	elseif state.pause then
		state.pause = false
		suspended(1, nextline)
	elseif state.stepOver_Target then
		local info = debug.getinfo(2, "f")
		local name = info.source

		if info.func == state.stepOver_Target or info.func == state.stepOver_Target_Prev then
			state.stepOver_Target = nil
			suspended(1, nextline)
		end
	end
end

table.insert(blt_debugger._read_message_handlers, function(msg)
	if msg.op ~= "breakpoints" then return end

	breakpoints = msg.breakpoints
	compile_breakpoints()

	--log("Got breakpoints: " .. json.encode(compiled_breakpoints))
end)

table.insert(blt_debugger._read_message_handlers, function(msg)
	if msg.op ~= "ide-action" then return end

	local action = msg.action

	if action == "pause" or action == "continue"
		or action == "stepOver" or action == "stepInto"
		or action == "runToCursor"
	then
		state[action] = true
		selected_line = msg["selected-line"]
	else
		error("Unknown IDE Action " .. tostring(action))
	end
end)

debug.sethook(hook_line, "l")

-- Read all our breakpoints
blt_debugger.send_message(json.encode({
	op = "get-breakpoints"
}))
while blt_debugger._last_message.op ~= "breakpoints" do
	log("[PDLab-DBG] Waiting for breakpoints...")
	blt_debugger.wait()
end

log("[PDLab-DBG] Done reading breakpoints")
