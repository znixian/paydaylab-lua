{
	"name": "Debugging",
	"description": "Debugging tools/functions",
	"author": "ZNix",
	"contact": "znix@znix.xyz",
	"version": "1.0",
	"blt_version": 2,
	"hooks": [
		{
			"hook_id" : "core/lib/system/coremodule",
			"script_path" : "main.lua"
		}
	]
}
